import base64
import random

from block_cipher.des import Des

valid_bool = {"yes": True, "y": True, "ye": True,
              "no": False, "n": False}
valid_decryption_bool = {"decryption": True, "d": True,
                         "encryption": False, "e": False}
valid_encoding = {"decryption": True, "d": True,
                         "encryption": False, "e": False}
response = {True: "Decryption", False: "Encryption"}

continue_loop = True
while continue_loop:
    print("DES using CBC   -   by Alberto Avalos")
    decrypt = input('Would you like to encrypt or decrypt your message [e/d]: ')
    try:
        decrypt = valid_decryption_bool[decrypt.lower().strip()]
    except KeyError as err:
        print("Response not valid. Restarting")
        continue
    key = input('Enter your 64 bit KEY as plaintext: ')

    iv = None
    if decrypt:
        iv_binary = input('Enter your 64 bit initialization vector as binary: ')
        iv = Des.to_byte_array(iv_binary.strip())
        message = input('Enter your encrypted message as binary: ')
        message = Des.to_byte_array(message.strip())
    else:
        message = input('Enter your message as plaintext: ')
        iv_binary = '{0:064b}'.format(random.getrandbits(64))
        print(iv_binary)
        iv = Des.to_byte_array(iv_binary)
    try:
        encryption_tool = Des(key, iv)
        encrypted_bits = encryption_tool.encrypt(message, decrypt)
        encrypted_bytes = Des.to_byte_array(encrypted_bits)
        encrypted_message_base_64 = base64.b64encode(encrypted_bytes)
        encrypted_message = "".join(map(chr, encrypted_bytes))
        print(response[decrypt] + " in plaintext:")
        print(encrypted_message)
        print(response[decrypt] + " encoded in Base64:")
        print(encrypted_message_base_64)
        print(response[decrypt] + " in bits:")
        print(encrypted_bits)
        print("Initialization vector used:")
        print(iv_binary)
        print("")
        print("Now going back. Reverting the " + response[decrypt])
        decrypted_bits = encryption_tool.encrypt(encrypted_bytes, not decrypt)
        print(response[not decrypt] + " in plaintext:")
        print((Des.to_byte_array(decrypted_bits)).decode())

    except ValueError as err:
        print(err)
        continue
    print(" ")

    continue_loop = input('Would you like to continue [y/n]: ')
    try:
        continue_loop = valid_bool[continue_loop.lower()]
    except KeyError:
        print("Response not valid. Restarting")
        continue
