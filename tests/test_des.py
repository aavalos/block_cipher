import unittest

from block_cipher.des import Des
from block_cipher.operation_mode import CipherBlockChaining


class TestDesMethods(unittest.TestCase):
    def test_des(self):
        bit_message = '0000000100100011010001010110011110001001101010111100110111101111'
        bit_key = '0001001100110100010101110111100110011011101111001101111111110001'

        byte_message = Des.to_byte_array(bit_message)
        byte_key = Des.to_byte_array(bit_key)

        encryption_tool = Des(byte_key)
        cbc = CipherBlockChaining(encryption_tool)
        encrypted_bits = cbc.encrypt(byte_message)
        encrypted_bytes = Des.to_byte_array(encrypted_bits)

        decrypted_bits = cbc.decrypt(encrypted_bytes)

        self.assertEqual(bit_message, decrypted_bits)

    def test_large_key(self):
        """key can not be larger than 64 bits"""
        key = '00010011001101000101011101111001100110111011110011011111111100011'
        string_key = str(Des.to_byte_array(key))
        with self.assertRaises(ValueError):
            Des(string_key)

    def test_small_key(self):
        """key can not be larger than 64 bits"""
        bit_message = '0000000100100011010001010110011110001001101010111100110111101111'
        bit_key = '000100110011'

        bytes_message = Des.to_byte_array(bit_message)
        bytes_key = Des.to_byte_array(bit_key)

        encryption_tool = Des(bytes_key)
        cbc = CipherBlockChaining(encryption_tool)
        encrypted_bits = cbc.encrypt(bytes_message)
        encrypted_bytes = Des.to_byte_array(encrypted_bits)
        self.assertNotEqual(bytes_message, encrypted_bytes)

        decrypted_bits = cbc.decrypt(encrypted_bytes)

        self.assertEqual(bit_message, decrypted_bits)

    def test_small_message(self):
        bit_message = '010001010110011110001001101010111100110111101111'
        bit_key = '0001001100110100010101110111100110011011101111001101111111110001'

        string_message = Des.to_byte_array(bit_message)
        string_key = Des.to_byte_array(bit_key)

        encryption_tool = Des(string_key)
        cbc = CipherBlockChaining(encryption_tool)
        encrypted_bits = cbc.encrypt(string_message)
        encrypted_message = Des.to_byte_array(encrypted_bits)

        decrypted_bits = cbc.decrypt(encrypted_message)

        self.assertEqual(int(''.join(bit_message), 2), int(''.join(decrypted_bits), 2))

    def test_cbc(self):
        message = 'SECRETTTAERF12GT'
        key = 'MSSGES'

        encryption_tool = Des(key)
        cbc = CipherBlockChaining(encryption_tool)
        encrypted_bits = cbc.encrypt(message)

        decrypted_bits = cbc.decrypt(Des.to_byte_array(encrypted_bits))
        decrypted_message = Des.to_byte_array(decrypted_bits).decode('utf-8')

        self.assertEqual(message, decrypted_message)


if __name__ == '__main__':
    unittest.main()
