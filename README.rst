=============
Block Cipher
=============


.. image:: https://img.shields.io/pypi/v/block_ciphers.svg
        :target: https://pypi.python.org/pypi/block_ciphers

.. image:: https://img.shields.io/travis/aavalos/block_ciphers.svg
        :target: https://travis-ci.org/aavalos/block_ciphers

.. image:: https://readthedocs.org/projects/block-ciphers/badge/?version=latest
        :target: https://block-ciphers.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/aavalos/block_ciphers/shield.svg
     :target: https://pyup.io/repos/github/aavalos/block_ciphers/
     :alt: Updates


Python implementation of DES and AES


* Free software: BSD license
* Documentation: https://block-ciphers.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

