# -*- coding: utf-8 -*-

"""Top-level package for Block Ciphers."""
from abc import ABC, abstractmethod

__author__ = """Alberto Avalos"""
__email__ = 'aaa.avalos@outlook.com'
__version__ = '0.1.0'


class BlockCipher(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def encrypt_block(self, block):
        pass

    @abstractmethod
    def decrypt_block(self, block):
        pass

    @abstractmethod
    def get_block_size(self):
        pass

    @abstractmethod
    def bit_string(self):
        pass

    @staticmethod
    @abstractmethod
    def xor_block(block1, block2):
        pass

    @staticmethod
    def rotate(l, n):
        return l[n:] + l[:n]

    @staticmethod
    def to_byte_array(binary_list):
        new_byte_array = bytearray()
        bit_blocks = [binary_list[x:x + 8] for x in
                      range(0, len(binary_list), 8)]
        for key, bit_block in enumerate(bit_blocks):
            # convert swapped bits back to a byte
            new_byte = int(''.join(bit_block), 2)
            # add new byte to returning list
            new_byte_array.append(new_byte)
        return new_byte_array

def get_binary_string(byte_array):
    binary_str = ""
    for b in byte_array:
        binary = '{0:08b}'.format(b)
        binary_str += binary
    return pad(binary_str)


def pad(bit_string):
    length = len(bit_string)
    remainder = length % 64
    if remainder != 0:
        pad_amount = 64 - remainder
        bit_string = bit_string.zfill(length + pad_amount)
    return bit_string


def xor_bit_string(bit_string_1, bit_string_2):
    xored = int(bit_string_1, 2) ^ int(bit_string_2, 2)
    return '{0:0{1}b}'.format(xored, len(bit_string_1))


def xor_byte_array(byte_array_1, byte_array_2):
    return [a ^ b for a, b in zip(byte_array_1, byte_array_2)]


