# -*- coding: utf-8 -*-
import itertools
import random

from block_cipher import get_binary_string


class CipherBlockChaining(object):
    def __init__(self, block_cipher, iv=None):
        self.__block_cipher = block_cipher
        bit_size = self.__block_cipher.get_block_size() * 8
        if iv:
            if not isinstance(iv, bytearray):
                iv = bytearray(iv, 'utf-8')
            iv_binary = get_binary_string(iv)
            if len(iv_binary) > bit_size:
                raise ValueError("Key can not be more than 64 bits.")
            self.__iv = iv_binary
        else:
            self.__iv = '{0:064b}'.format(random.getrandbits(bit_size))
        if not self.__block_cipher.bit_string():
            self.__iv = self.__block_cipher.to_byte_array(self.__iv)

    def encrypt(self, plaintext):
        # break into blocks of 8 bytes (64 bits)
        blocks = self.block_split(plaintext)
        encrypted_blocks = []
        # First round.
        block = self.__block_cipher.xor_block(blocks[0], self.__iv)
        encrypted_block = self.__block_cipher.encrypt_block(block)
        encrypted_blocks.append(encrypted_block)
        # Remaining rounds.
        for block in blocks[1:]:
            block = self.__block_cipher.xor_block(block, encrypted_blocks[-1])
            encrypted_block = self.__block_cipher.encrypt_block(block)
            encrypted_blocks.append(encrypted_block)

        if self.__block_cipher.bit_string():
            encrypted_string = ''.join(
                [encypted_block_64 for encypted_block_64 in encrypted_blocks])
            return encrypted_string
        return bytearray(itertools.chain.from_iterable(encrypted_blocks))

    def decrypt(self, plaintext):
        # break into blocks of 8 bytes (64 bits)
        blocks = self.block_split(plaintext)
        decrypted_blocks = []
        # First round.
        encrypted_block = self.__block_cipher.decrypt_block(blocks[0])
        encrypted_block = self.__block_cipher.xor_block(encrypted_block, self.__iv)
        decrypted_blocks.append(encrypted_block)
        for i, block in enumerate(blocks[1:], start=1):
            encrypted_block = self.__block_cipher.decrypt_block(block)
            encrypted_block = self.__block_cipher.xor_block(encrypted_block, blocks[i - 1])
            decrypted_blocks.append(encrypted_block)
        if self.__block_cipher.bit_string():
            decrypted_string = ''.join(
                [encypted_block_64 for encypted_block_64 in decrypted_blocks])

            return decrypted_string
        d_bytes = bytearray(itertools.chain.from_iterable(decrypted_blocks))
        return d_bytes.decode('utf-8').rstrip('\x00')

    def block_split(self, in_stream):
        # break into blocks of 8 bytes (64 bits)
        if not isinstance(in_stream, bytearray):
            in_stream = bytearray(in_stream, 'utf-8')

        size = self.__block_cipher.get_block_size()
        if self.__block_cipher.bit_string():
            in_stream = get_binary_string(in_stream)
            size *= 8

        blocks = [in_stream[x:x + size] for x in
                  range(0, len(in_stream), size)]

        while len(blocks[-1]) < size:
            blocks[-1].append(0x00)
        return blocks
